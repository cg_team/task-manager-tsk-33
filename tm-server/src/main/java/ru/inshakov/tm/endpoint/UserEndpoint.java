package ru.inshakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.endpoint.IUserEndpoint;
import ru.inshakov.tm.api.service.IServiceLocator;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    private IUserService userService;

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    public boolean isLoginExists(@Nullable String login) {
        return userService.isLoginExists(login);
    }

    @Nullable
    @WebMethod
    public User findByEmail(
            @Nullable @WebParam(name="email", partName = "email") String email
    ) {
        return userService.findByEmail(email);
    }

    @Nullable
    @WebMethod
    public User findByLogin(
            @Nullable @WebParam(name="login", partName = "login") String login
    ) {
        return userService.findByLogin(login);
    }

    public boolean isEmailExists(@Nullable String email) {
        return userService.isEmailExists(email);
    }

    @WebMethod
    public void removeByLogin(
            @Nullable @WebParam(name="login", partName = "login") String login
    ) {
        userService.removeByLogin(login);
    }

    @NotNull
    @WebMethod
    public User createUser(
            @Nullable @WebParam(name="login", partName = "login") String login,
            @Nullable @WebParam(name="password", partName = "password") String password
    ) {
        return userService.create(login, password);
    }

    @WebMethod
    public void createUserWithEmail(
            @Nullable @WebParam(name="login", partName = "login") String login,
            @Nullable @WebParam(name="password", partName = "password") String password,
            @Nullable @WebParam(name="email", partName = "email") String email
    ) {
        userService.create(login, password, email);
    }

    @WebMethod
    public void createUserWithRole(
            @Nullable @WebParam(name="login", partName = "login") String login,
            @Nullable @WebParam(name="password", partName = "password") String password,
            @Nullable @WebParam(name="role", partName = "role") Role role
    ) {
        userService.create(login, password, role);
    }

    @WebMethod
    public void setPassword(
            @Nullable @WebParam(name="userId", partName = "userId") String userId,
            @Nullable @WebParam(name="password", partName = "password") String password
    ) {
        userService.setPassword(userId, password);
    }

    @WebMethod
    public void updateUser(
            @Nullable @WebParam(name="userId", partName = "userId") String userId,
            @Nullable @WebParam(name="firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name="secondName", partName = "secondName") String secondName,
            @Nullable @WebParam(name="middleName", partName = "middleName") String middleName
    ) {
        userService.updateUser(userId, firstName, secondName, middleName);
    }

    @WebMethod
    public void lockUserByLogin(
            @Nullable @WebParam(name="login", partName = "login") String login
    ) {
        userService.lockUserByLogin(login);
    }

    @WebMethod
    public void unlockUserByLogin(
            @Nullable @WebParam(name="login", partName = "login") String login
    ) {
        userService.unlockUserByLogin(login);
    }

    @WebMethod
    public void add(
            @NotNull @WebParam(name="entity", partName = "entity") User entity
    ) {
        userService.add(entity);
    }

    @WebMethod
    public void clear() {
        userService.clear();
    }

    @Nullable
    @WebMethod
    public List<User> findAll() {
        return userService.findAll();
    }

    @Nullable
    @WebMethod
    public User findById(
            @NotNull @WebParam(name="id", partName = "id") String id
    ) {
        return userService.findById(id);
    }

    @WebMethod
    public void remove(
            @NotNull @WebParam(name="entity", partName = "entity") User entity
    ) {
        userService.remove(entity);
    }

    @Nullable
    @WebMethod
    public User removeById(
            @NotNull @WebParam(name="id", partName = "id") String id
    ) {
        return userService.removeById(id);
    }

    @WebMethod
    public void addAll(
            @Nullable @WebParam(name="entity", partName = "entity") List<User> entity
    ) {
        userService.addAll(entity);
    }
}
