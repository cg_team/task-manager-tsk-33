package ru.inshakov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class CalcEndpoint {

    @WebMethod
    public int sum(
            @WebParam(name = "a", partName = "a") int a,
            @WebParam(name = "b", partName = "b") int b
    ) {
        return a + b;
    }

}
