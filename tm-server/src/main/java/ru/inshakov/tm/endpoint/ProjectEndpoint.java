package ru.inshakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.IProjectService;
import ru.inshakov.tm.api.service.IServiceLocator;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.entity.AccessForbiddenException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    private IProjectService projectService;

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @NotNull
    @WebMethod
    public Project addProjectByUserId(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @NotNull @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="description", partName = "description") String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.add(session.getUserId(), name, description);
    }


    @NotNull
    @WebMethod
    public Project changeProjectStatusById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id,
            @Nullable @WebParam(name="status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.changeStatusById(session.getUserId(), id, status);
    }

    @NotNull
    @WebMethod
    public Project changeProjectStatusByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index,
            @Nullable @WebParam(name="status", partName = "status") Status status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @NotNull
    @WebMethod
    public Project changeProjectStatusByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.changeStatusByName(session.getUserId(), name, status);
    }

    @WebMethod
    public void clearProjectByUserId(
            @NotNull @WebParam(name="session", partName = "session") Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        projectService.clear(session.getUserId());
    }

    @NotNull
    @WebMethod
    public Project finishProjectById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishById(session.getUserId(), id);
    }

    @NotNull
    @WebMethod
    public Project finishProjectByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishByIndex(session.getUserId(), index);
    }

    @NotNull
    @WebMethod
    public Project finishProjectByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishByName(session.getUserId(), name);
    }

    //@Nullable
    //@WebMethod
    //public List<Project> findAll(@NotNull String userId, @Nullable Comparator<Project> comparator) {
    //    return projectService.findAll(userId, comparator);
    //}

    @Nullable
    @WebMethod
    public List<Project> findAllProjectsByUserId(
            @NotNull @WebParam(name="session", partName = "session") Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Nullable
    @WebMethod
    public Project findOneProjectById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    public Project findOneProjectByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    public Project findOneProjectByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    public Project removeProjectOneById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    public Project removeProjectOneByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    public Project removeProjectOneByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeOneByName(session.getUserId(), name);
    }

    @NotNull
    @WebMethod
    public Project startProjectById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @NotNull
    @WebMethod
    public Project startProjectByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @NotNull
    @WebMethod
    public Project startProjectByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @NotNull
    @WebMethod
    public Project updateProjectById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="description", partName = "description") String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateById(session.getUserId(), id, name, description);
    }

    @NotNull
    @WebMethod
    public Project updateProjectByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="description", partName = "description") String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public void clearProject() {
        projectService.clear();
    }

    @Nullable
    @WebMethod
    public List<Project> findAllProjects() {
        return projectService.findAll();
    }

    @Nullable
    @WebMethod
    public Project findProjectById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @NotNull @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findById(id);
    }

    @WebMethod
    public void removeProject(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @NotNull @WebParam(name="entity", partName = "entity") Project entity
    ) {
        projectService.remove(entity);
    }

    @Nullable
    @WebMethod
    public Project removeProjectById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @NotNull @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeById(id);
    }

    @WebMethod
    public void addAllProjects(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="entity", partName = "entity") List<Project> entity
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        projectService.addAll(entity);
    }
}
