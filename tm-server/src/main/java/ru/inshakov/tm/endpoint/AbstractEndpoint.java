package ru.inshakov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.IServiceLocator;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
public abstract class AbstractEndpoint {

    @NotNull
    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@Nullable final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
