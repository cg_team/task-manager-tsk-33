package ru.inshakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.IServiceLocator;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.entity.AccessForbiddenException;
import ru.inshakov.tm.model.Session;
import ru.inshakov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint {

    private ITaskService taskService;

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @NotNull
    @WebMethod
    public Task addTaskByUserId(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="description", partName = "description") String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.add(session.getUserId(), name, description);
    }

    @NotNull
    @WebMethod
    public Task changeTaskStatusById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id,
            @Nullable @WebParam(name="status", partName = "status") Status status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.changeStatusById(session.getUserId(), id, status);
    }

    @NotNull
    @WebMethod
    public Task changeTaskStatusByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index,
            @Nullable @WebParam(name="status", partName = "status") Status status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @NotNull
    @WebMethod
    public Task changeTaskStatusByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="status", partName = "status") Status status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.changeStatusByName(session.getUserId(), name, status);
    }

    @WebMethod
    public void clearTaskByUserId(
            @NotNull @WebParam(name="session", partName = "session") Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        taskService.clear(session.getUserId());
    }

    @NotNull
    @WebMethod
    public Task finishTaskById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishById(session.getUserId(), id);
    }

    @NotNull
    @WebMethod
    public Task finishTaskByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishByIndex(session.getUserId(), index);
    }

    @NotNull
    @WebMethod
    public Task finishTaskByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishByName(session.getUserId(), name);
    }

    //@Nullable
   // public @Nullable List<Task> findAll(@NotNull String userId, @Nullable Comparator<Task> comparator) {
     //   return taskService.findAll(userId, comparator);
    //}

    @Nullable
    @WebMethod
    public List<Task> findAllTasksByUserId(
            @NotNull @WebParam(name="session", partName = "session") Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Nullable
    @WebMethod
    public Task findOneTaskById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    public Task findOneTaskByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    public Task findOneTaskByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    public Task removeOneTaskById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    public Task removeOneTaskByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    public Task removeOneTaskByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @NotNull
    @WebMethod
    public Task startTaskById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @NotNull
    @WebMethod
    public Task startTaskByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @NotNull
    @WebMethod
    public Task startTaskByName(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="name", partName = "name") String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @NotNull
    @WebMethod
    public Task updateTaskById(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="id", partName = "id") String id,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="description", partName = "description") String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.updateById(session.getUserId(), id, name, description);
    }

    @NotNull
    @WebMethod
    public Task updateTaskByIndex(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @Nullable @WebParam(name="index", partName = "index") Integer index,
            @Nullable @WebParam(name="name", partName = "name") String name,
            @Nullable @WebParam(name="description", partName = "description") String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public void addTask(
            @NotNull @WebParam(name="session", partName = "session") Session session,
            @NotNull @WebParam(name="entity", partName = "entity") Task entity
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        taskService.add(entity);
    }

    @WebMethod
    public void clearTasks() {
        taskService.clear();
    }

    @Nullable
    @WebMethod
    public List<Task> findAllTasks() {
        return taskService.findAll();
    }

    @Nullable
    @WebMethod
    public Task findTaskById(
            @NotNull @WebParam(name="id", partName = "id") String id
    ) {
        return taskService.findById(id);
    }

    @WebMethod
    public void removeTask(
            @NotNull Task entity
    ) {
        taskService.remove(entity);
    }

    @Nullable
    @WebMethod
    public Task removeTaskById(
            @NotNull @WebParam(name="id", partName = "id") String id
    ) {
        return taskService.removeById(id);
    }

    @WebMethod
    public void addAllTasks(
            @Nullable @WebParam(name="entity", partName = "entity") List<Task> entity
    ) {
        taskService.addAll(entity);
    }

    @NotNull
    @WebMethod
    public Task bindTaskByProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "projectId", partName = "projectId") final String projectId,
            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId
    ) {
        return serviceLocator.getProjectTaskService().bindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @NotNull
    @WebMethod
    public Task unbindTaskByProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "projectId", partName = "projectId") final String projectId,
            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId
    ) {
        return serviceLocator.getProjectTaskService().unbindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @Nullable
    @WebMethod
    public List<Task> findAllTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "projectId", partName = "projectId") final String projectId
    ) {
        return serviceLocator.getProjectTaskService().findAllTasksByProjectId(session.getUserId(), projectId);
    }

}
