package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.ISessionRepository;
import ru.inshakov.tm.model.Session;

public final class SessionRepository extends AbstractRepository<Session>
        implements ISessionRepository {

}