package ru.inshakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class DataService implements IDataService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DataService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @Override
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        @NotNull final IProjectService projectService =
                serviceLocator.getProjectService();
        projectService.clear();
        projectService.addAll(domain.getProjects());
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        taskService.clear();
        taskService.addAll(domain.getTasks());
        @NotNull final IUserService userService =
                serviceLocator.getUserService();
        userService.clear();
        userService.addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    @Override
    @SneakyThrows
    public void loadBackupCommand(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileXmlPath("backup");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        if (!file.exists()) return;
        @NotNull final String xml = new String(
                Files.readAllBytes(Paths.get(filePath))
        );
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain =
                objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataBase64Command(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileBase64Path();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @Nullable final String base64Date =
                new String(Files.readAllBytes(Paths.get(filePath)));
        @Nullable final byte[] bytes =
                new BASE64Decoder().decodeBuffer(base64Date);
        @NotNull final ByteArrayInputStream byteArrayInputStream =
                new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream =
                new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataBinaryCommand(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileBinaryPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final FileInputStream fileInputStream =
                new FileInputStream(filePath);
        @NotNull final ObjectInputStream objectInputStream =
                new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataJsonFasterXmlCommand(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService()
                        .getFileJsonPath("fasterXml");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final String json = new String(
                Files.readAllBytes(Paths.get(filePath))
        );
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain =
                objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataJsonJaxBCommand(
    ) {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @Nullable final String filePath =
                serviceLocator.getPropertyService()
                        .getFileJsonPath("jaxb");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller =
                jaxbContext.createUnmarshaller();
        unmarshaller.setProperty("eclipselink.media-type", "application/json");
        @NotNull final File file = new File(filePath);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataXmlFasterXmlCommand(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileXmlPath("fasterXml");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final String xml = new String(
                Files.readAllBytes(Paths.get(filePath))
        );
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain =
                objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataXmlJaxBCommand(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService()
                        .getFileXmlPath("jaxb");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller =
                jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(filePath);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataYamlFasterXmlCommand(
    ) {
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileYamlPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final String json = new String(
                Files.readAllBytes(Paths.get(filePath))
        );
        @NotNull final ObjectMapper objectMapper =
                new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain =
                objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveBackupCommand(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileXmlPath("backup");;
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBase64Command(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileBase64Path();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ByteArrayOutputStream byteArrayOutputStream =
                new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(filePath);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBinaryCommand(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileBinaryPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJsonFasterXmlCommand(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService()
                        .getFileJsonPath("fasterXml");;
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJsonJaxBCommand(
    ) {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService()
                        .getFileJsonPath("jaxb");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller =
                jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty("eclipselink.media-type", "application/json");
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataXmlFasterXmlCommand(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileXmlPath("fasterXml");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataXmlJaxBCommand(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService()
                        .getFileXmlPath("jaxb");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller =
                jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataYamlFasterXmlCommand(
    ) {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileYamlPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper =
                new ObjectMapper(new YAMLFactory());
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
