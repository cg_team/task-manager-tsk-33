package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

}
