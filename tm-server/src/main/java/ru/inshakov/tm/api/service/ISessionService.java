package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.entity.AccessForbiddenException;
import ru.inshakov.tm.model.Session;
import ru.inshakov.tm.model.User;

public interface ISessionService {

    @Nullable
    User checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull Session session) throws AccessForbiddenException;

    @Nullable
    Session open(@NotNull String login, @NotNull String password);

    @Nullable
    Session sign(@Nullable Session session);

    void validate(@Nullable Session session) throws AccessForbiddenException;

    void validate(@Nullable Session session, @Nullable Role role)
            throws AccessForbiddenException;

}
