package ru.inshakov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Domain;

public interface IDataService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void loadBackupCommand();

    @SneakyThrows
    void loadDataBase64Command();

    @SneakyThrows
    void loadDataBinaryCommand();

    @SneakyThrows
    void loadDataJsonFasterXmlCommand();

    @SneakyThrows
    void loadDataJsonJaxBCommand();

    @SneakyThrows
    void loadDataXmlFasterXmlCommand();

    @SneakyThrows
    void loadDataXmlJaxBCommand();

    @SneakyThrows
    void loadDataYamlFasterXmlCommand();

    @SneakyThrows
    void saveBackupCommand();

    @SneakyThrows
    void saveDataBase64Command();

    @SneakyThrows
    void saveDataBinaryCommand();

    @SneakyThrows
    void saveDataJsonFasterXmlCommand();

    @SneakyThrows
    void saveDataJsonJaxBCommand();

    @SneakyThrows
    void saveDataXmlFasterXmlCommand();

    @SneakyThrows
    void saveDataXmlJaxBCommand();

    @SneakyThrows
    void saveDataYamlFasterXmlCommand();

}
