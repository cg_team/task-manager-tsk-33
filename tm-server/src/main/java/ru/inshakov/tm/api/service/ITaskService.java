package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

}
