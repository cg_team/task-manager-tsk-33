package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project add(@NotNull String userId, @NotNull String name, @Nullable String description);

}
