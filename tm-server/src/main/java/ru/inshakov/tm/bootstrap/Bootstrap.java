package ru.inshakov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.*;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.repository.*;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ILoggerService logService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(this, sessionRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository,taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final IDataService dataService = new DataService(this);

    @SneakyThrows
    private void initCommands(){
        @NotNull final Reflections reflections = new Reflections("ru.inshakov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.inshakov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz:classes){
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    private void initEndpoints(){
        @NotNull final Reflections reflections = new Reflections("ru.inshakov.tm.endpoint");
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        for (@NotNull final Class<? extends AbstractEndpoint> clazz:classes){
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    public void init() {
        //initUsers();
        initPID();
        initEndpoints();
    }

    private void initUsers() {
        userService.create("test1", "test1", "test1@test.com");
        userService.create("test2", "test2", "test2@test.com");
        userService.create("test3", "test3", "test3@test.com");
        userService.create("test4", "test4", "test4@test.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl,endpoint);
    }

}
