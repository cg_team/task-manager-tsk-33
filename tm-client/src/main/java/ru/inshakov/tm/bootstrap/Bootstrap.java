package ru.inshakov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.inshakov.tm.api.service.IPropertyService;
import ru.inshakov.tm.api.repository.ICommandRepository;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.exception.system.UnknownArgumentException;
import ru.inshakov.tm.exception.system.UnknownCommandException;
import ru.inshakov.tm.repository.CommandRepository;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.util.SystemUtil;
import ru.inshakov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @Nullable private Session session;

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final DataEndpointService dataEndpointService = new DataEndpointService();

    @NotNull
    private final DataEndpoint dataEndpoint = dataEndpointService.getDataEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    public void init() {
        initCommands();
        initUsers();
        initPID();
    }

    @SneakyThrows
    private void initCommands(){
        @NotNull final Reflections reflections = new Reflections("ru.inshakov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.inshakov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz:classes){
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }


    private void initUsers() {
        //@NotNull final Session session = sessionEndpoint.openSession("admin","admin");
        //userEndpoint.createUserWithRole("admin1","admin1", Role.ADMIN);
        //@NotNull final Session session = sessionEndpoint.openSession("admin","admin");
        //userEndpoint.createUserWithEmail("test","test","test@test.ru");
        //projectEndpoint.addProjectByUserId(session, "project1", "project1");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run(@Nullable final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        init();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        Optional.ofNullable(command).orElseThrow(() -> new UnknownArgumentException(arg));
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).filter(item -> item.length != 0).isPresent()) return false;
        String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String name) {
        if (!Optional.ofNullable(name).isPresent()) return;
        final AbstractCommand command = commandService.getCommandByName(name);
        Optional.ofNullable(command).orElseThrow(() -> new UnknownCommandException(name));
        command.execute();
    }

}
