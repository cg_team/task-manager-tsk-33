
package ru.inshakov.tm.endpoint;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "UserEndpoint", targetNamespace = "http://endpoint.tm.inshakov.ru/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface UserEndpoint {


    /**
     * 
     * @param entity
     */
    @WebMethod
    @RequestWrapper(localName = "add", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.Add")
    @ResponseWrapper(localName = "addResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/addRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/addResponse")
    public void add(
        @WebParam(name = "entity", targetNamespace = "")
        User entity);

    /**
     * 
     * @param entity
     */
    @WebMethod
    @RequestWrapper(localName = "remove", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.Remove")
    @ResponseWrapper(localName = "removeResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/removeRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/removeResponse")
    public void remove(
        @WebParam(name = "entity", targetNamespace = "")
        User entity);

    /**
     * 
     */
    @WebMethod
    @RequestWrapper(localName = "clear", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.Clear")
    @ResponseWrapper(localName = "clearResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ClearResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/clearRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/clearResponse")
    public void clear();

    /**
     * 
     * @param entity
     */
    @WebMethod
    @RequestWrapper(localName = "addAll", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddAll")
    @ResponseWrapper(localName = "addAllResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddAllResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/addAllRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/addAllResponse")
    public void addAll(
        @WebParam(name = "entity", targetNamespace = "")
        List<User> entity);

    /**
     * 
     * @param login
     * @return
     *     returns ru.inshakov.tm.endpoint.User
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findByLogin", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindByLogin")
    @ResponseWrapper(localName = "findByLoginResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindByLoginResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/findByLoginRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/findByLoginResponse")
    public User findByLogin(
        @WebParam(name = "login", targetNamespace = "")
        String login);

    /**
     * 
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.User
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindById")
    @ResponseWrapper(localName = "findByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/findByIdRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/findByIdResponse")
    public User findById(
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @return
     *     returns java.util.List<ru.inshakov.tm.endpoint.User>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAll", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindAll")
    @ResponseWrapper(localName = "findAllResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindAllResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/findAllRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/findAllResponse")
    public List<User> findAll();

    /**
     * 
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.User
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "removeById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveById")
    @ResponseWrapper(localName = "removeByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/removeByIdRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/removeByIdResponse")
    public User removeById(
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @param login
     */
    @WebMethod
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UnlockUserByLoginResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/unlockUserByLoginResponse")
    public void unlockUserByLogin(
        @WebParam(name = "login", targetNamespace = "")
        String login);

    /**
     * 
     * @param login
     */
    @WebMethod
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LockUserByLoginResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/lockUserByLoginResponse")
    public void lockUserByLogin(
        @WebParam(name = "login", targetNamespace = "")
        String login);

    /**
     * 
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "isLoginExists", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.IsLoginExists")
    @ResponseWrapper(localName = "isLoginExistsResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.IsLoginExistsResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/isLoginExistsRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/isLoginExistsResponse")
    public boolean isLoginExists(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param firstName
     * @param middleName
     * @param userId
     * @param secondName
     */
    @WebMethod
    @RequestWrapper(localName = "updateUser", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UpdateUser")
    @ResponseWrapper(localName = "updateUserResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UpdateUserResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/updateUserRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/updateUserResponse")
    public void updateUser(
        @WebParam(name = "userId", targetNamespace = "")
        String userId,
        @WebParam(name = "firstName", targetNamespace = "")
        String firstName,
        @WebParam(name = "secondName", targetNamespace = "")
        String secondName,
        @WebParam(name = "middleName", targetNamespace = "")
        String middleName);

    /**
     * 
     * @param password
     * @param userId
     */
    @WebMethod
    @RequestWrapper(localName = "setPassword", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SetPassword")
    @ResponseWrapper(localName = "setPasswordResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SetPasswordResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/setPasswordRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/setPasswordResponse")
    public void setPassword(
        @WebParam(name = "userId", targetNamespace = "")
        String userId,
        @WebParam(name = "password", targetNamespace = "")
        String password);

    /**
     * 
     * @param email
     * @return
     *     returns ru.inshakov.tm.endpoint.User
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findByEmail", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindByEmail")
    @ResponseWrapper(localName = "findByEmailResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindByEmailResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/findByEmailRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/findByEmailResponse")
    public User findByEmail(
        @WebParam(name = "email", targetNamespace = "")
        String email);

    /**
     * 
     * @param login
     */
    @WebMethod
    @RequestWrapper(localName = "removeByLogin", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveByLogin")
    @ResponseWrapper(localName = "removeByLoginResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveByLoginResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/removeByLoginRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/removeByLoginResponse")
    public void removeByLogin(
        @WebParam(name = "login", targetNamespace = "")
        String login);

    /**
     * 
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "isEmailExists", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.IsEmailExists")
    @ResponseWrapper(localName = "isEmailExistsResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.IsEmailExistsResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/isEmailExistsRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/isEmailExistsResponse")
    public boolean isEmailExists(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param password
     * @param login
     * @param email
     */
    @WebMethod
    @RequestWrapper(localName = "createUserWithEmail", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.CreateUserWithEmail")
    @ResponseWrapper(localName = "createUserWithEmailResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.CreateUserWithEmailResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/createUserWithEmailRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/createUserWithEmailResponse")
    public void createUserWithEmail(
        @WebParam(name = "login", targetNamespace = "")
        String login,
        @WebParam(name = "password", targetNamespace = "")
        String password,
        @WebParam(name = "email", targetNamespace = "")
        String email);

    /**
     * 
     * @param password
     * @param role
     * @param login
     */
    @WebMethod
    @RequestWrapper(localName = "createUserWithRole", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.CreateUserWithRole")
    @ResponseWrapper(localName = "createUserWithRoleResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.CreateUserWithRoleResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/createUserWithRoleRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/createUserWithRoleResponse")
    public void createUserWithRole(
        @WebParam(name = "login", targetNamespace = "")
        String login,
        @WebParam(name = "password", targetNamespace = "")
        String password,
        @WebParam(name = "role", targetNamespace = "")
        Role role);

    /**
     * 
     * @param password
     * @param login
     * @return
     *     returns ru.inshakov.tm.endpoint.User
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.CreateUserResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/UserEndpoint/createUserRequest", output = "http://endpoint.tm.inshakov.ru/UserEndpoint/createUserResponse")
    public User createUser(
        @WebParam(name = "login", targetNamespace = "")
        String login,
        @WebParam(name = "password", targetNamespace = "")
        String password);

}
