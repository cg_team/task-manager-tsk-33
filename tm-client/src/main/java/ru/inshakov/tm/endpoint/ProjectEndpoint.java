
package ru.inshakov.tm.endpoint;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ProjectEndpoint", targetNamespace = "http://endpoint.tm.inshakov.ru/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ProjectEndpoint {


    /**
     * 
     * @param session
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectByIdResponse")
    public Project removeProjectById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @param session
     * @param entity
     */
    @WebMethod
    @RequestWrapper(localName = "addAllProjects", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddAllProjects")
    @ResponseWrapper(localName = "addAllProjectsResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddAllProjectsResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/addAllProjectsRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/addAllProjectsResponse")
    public void addAllProjects(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "entity", targetNamespace = "")
        List<Project> entity);

    /**
     * 
     * @param session
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findProjectById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindProjectById")
    @ResponseWrapper(localName = "findProjectByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindProjectByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findProjectByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findProjectByIdResponse")
    public Project findProjectById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @param session
     * @param entity
     */
    @WebMethod
    @RequestWrapper(localName = "removeProject", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProject")
    @ResponseWrapper(localName = "removeProjectResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectResponse")
    public void removeProject(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "entity", targetNamespace = "")
        Project entity);

    /**
     * 
     * @param session
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "startProjectById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.StartProjectById")
    @ResponseWrapper(localName = "startProjectByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.StartProjectByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/startProjectByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/startProjectByIdResponse")
    public Project startProjectById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     */
    @WebMethod
    @RequestWrapper(localName = "clearProject", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ClearProject")
    @ResponseWrapper(localName = "clearProjectResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ClearProjectResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/clearProjectRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/clearProjectResponse")
    public void clearProject();

    /**
     * 
     * @return
     *     returns java.util.List<ru.inshakov.tm.endpoint.Project>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAllProjects", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindAllProjects")
    @ResponseWrapper(localName = "findAllProjectsResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindAllProjectsResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findAllProjectsRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findAllProjectsResponse")
    public List<Project> findAllProjects();

    /**
     * 
     * @param session
     * @param name
     * @param description
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "addProjectByUserId", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddProjectByUserId")
    @ResponseWrapper(localName = "addProjectByUserIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.AddProjectByUserIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/addProjectByUserIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/addProjectByUserIdResponse")
    public Project addProjectByUserId(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "name", targetNamespace = "")
        String name,
        @WebParam(name = "description", targetNamespace = "")
        String description);

    /**
     * 
     * @param session
     * @param id
     * @param status
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "changeProjectStatusById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ChangeProjectStatusById")
    @ResponseWrapper(localName = "changeProjectStatusByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ChangeProjectStatusByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/changeProjectStatusByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/changeProjectStatusByIdResponse")
    public Project changeProjectStatusById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id,
        @WebParam(name = "status", targetNamespace = "")
        Status status);

    /**
     * 
     * @param session
     * @param index
     * @param status
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "changeProjectStatusByIndex", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ChangeProjectStatusByIndex")
    @ResponseWrapper(localName = "changeProjectStatusByIndexResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ChangeProjectStatusByIndexResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/changeProjectStatusByIndexRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/changeProjectStatusByIndexResponse")
    public Project changeProjectStatusByIndex(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "index", targetNamespace = "")
        Integer index,
        @WebParam(name = "status", targetNamespace = "")
        Status status);

    /**
     * 
     * @param session
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "finishProjectById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FinishProjectById")
    @ResponseWrapper(localName = "finishProjectByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FinishProjectByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/finishProjectByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/finishProjectByIdResponse")
    public Project finishProjectById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @param session
     * @param index
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "finishProjectByIndex", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FinishProjectByIndex")
    @ResponseWrapper(localName = "finishProjectByIndexResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FinishProjectByIndexResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/finishProjectByIndexRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/finishProjectByIndexResponse")
    public Project finishProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "index", targetNamespace = "")
        Integer index);

    /**
     * 
     * @param session
     * @return
     *     returns java.util.List<ru.inshakov.tm.endpoint.Project>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAllProjectsByUserId", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindAllProjectsByUserId")
    @ResponseWrapper(localName = "findAllProjectsByUserIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindAllProjectsByUserIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findAllProjectsByUserIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findAllProjectsByUserIdResponse")
    public List<Project> findAllProjectsByUserId(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "clearProjectByUserId", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ClearProjectByUserId")
    @ResponseWrapper(localName = "clearProjectByUserIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ClearProjectByUserIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/clearProjectByUserIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/clearProjectByUserIdResponse")
    public void clearProjectByUserId(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     * @param name
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "finishProjectByName", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FinishProjectByName")
    @ResponseWrapper(localName = "finishProjectByNameResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FinishProjectByNameResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/finishProjectByNameRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/finishProjectByNameResponse")
    public Project finishProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "name", targetNamespace = "")
        String name);

    /**
     * 
     * @param session
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findOneProjectById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindOneProjectById")
    @ResponseWrapper(localName = "findOneProjectByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindOneProjectByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findOneProjectByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findOneProjectByIdResponse")
    public Project findOneProjectById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @param session
     * @param index
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findOneProjectByIndex", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindOneProjectByIndex")
    @ResponseWrapper(localName = "findOneProjectByIndexResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindOneProjectByIndexResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findOneProjectByIndexRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findOneProjectByIndexResponse")
    public Project findOneProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "index", targetNamespace = "")
        Integer index);

    /**
     * 
     * @param session
     * @param name
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findOneProjectByName", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindOneProjectByName")
    @ResponseWrapper(localName = "findOneProjectByNameResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.FindOneProjectByNameResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findOneProjectByNameRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/findOneProjectByNameResponse")
    public Project findOneProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "name", targetNamespace = "")
        String name);

    /**
     * 
     * @param session
     * @param name
     * @param status
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "changeProjectStatusByName", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ChangeProjectStatusByName")
    @ResponseWrapper(localName = "changeProjectStatusByNameResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.ChangeProjectStatusByNameResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/changeProjectStatusByNameRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/changeProjectStatusByNameResponse")
    public Project changeProjectStatusByName(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "name", targetNamespace = "")
        String name,
        @WebParam(name = "status", targetNamespace = "")
        Status status);

    /**
     * 
     * @param session
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "removeProjectOneById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectOneById")
    @ResponseWrapper(localName = "removeProjectOneByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectOneByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectOneByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectOneByIdResponse")
    public Project removeProjectOneById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id);

    /**
     * 
     * @param session
     * @param index
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "removeProjectOneByIndex", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectOneByIndex")
    @ResponseWrapper(localName = "removeProjectOneByIndexResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectOneByIndexResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectOneByIndexRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectOneByIndexResponse")
    public Project removeProjectOneByIndex(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "index", targetNamespace = "")
        Integer index);

    /**
     * 
     * @param session
     * @param index
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "startProjectByIndex", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.StartProjectByIndex")
    @ResponseWrapper(localName = "startProjectByIndexResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.StartProjectByIndexResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/startProjectByIndexRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/startProjectByIndexResponse")
    public Project startProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "index", targetNamespace = "")
        Integer index);

    /**
     * 
     * @param session
     * @param name
     * @param description
     * @param id
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UpdateProjectByIdResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/updateProjectByIdResponse")
    public Project updateProjectById(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "id", targetNamespace = "")
        String id,
        @WebParam(name = "name", targetNamespace = "")
        String name,
        @WebParam(name = "description", targetNamespace = "")
        String description);

    /**
     * 
     * @param session
     * @param name
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "removeProjectOneByName", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectOneByName")
    @ResponseWrapper(localName = "removeProjectOneByNameResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.RemoveProjectOneByNameResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectOneByNameRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/removeProjectOneByNameResponse")
    public Project removeProjectOneByName(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "name", targetNamespace = "")
        String name);

    /**
     * 
     * @param session
     * @param name
     * @param index
     * @param description
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.UpdateProjectByIndexResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/updateProjectByIndexResponse")
    public Project updateProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "index", targetNamespace = "")
        Integer index,
        @WebParam(name = "name", targetNamespace = "")
        String name,
        @WebParam(name = "description", targetNamespace = "")
        String description);

    /**
     * 
     * @param session
     * @param name
     * @return
     *     returns ru.inshakov.tm.endpoint.Project
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "startProjectByName", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.StartProjectByName")
    @ResponseWrapper(localName = "startProjectByNameResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.StartProjectByNameResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/startProjectByNameRequest", output = "http://endpoint.tm.inshakov.ru/ProjectEndpoint/startProjectByNameResponse")
    public Project startProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        Session session,
        @WebParam(name = "name", targetNamespace = "")
        String name);

}
