
package ru.inshakov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.inshakov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddTask_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "addTask");
    private final static QName _RemoveTaskResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeTaskResponse");
    private final static QName _StartTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "startTaskByIdResponse");
    private final static QName _FinishTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "finishTaskByIndexResponse");
    private final static QName _FinishTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "finishTaskById");
    private final static QName _RemoveOneTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeOneTaskByIdResponse");
    private final static QName _FindTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findTaskByIdResponse");
    private final static QName _BindTaskByProject_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "bindTaskByProject");
    private final static QName _ChangeTaskStatusByName_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "changeTaskStatusByName");
    private final static QName _ChangeTaskStatusByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "changeTaskStatusByIdResponse");
    private final static QName _StartTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "startTaskByIndexResponse");
    private final static QName _AddAllTasks_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "addAllTasks");
    private final static QName _ChangeTaskStatusByIndex_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "changeTaskStatusByIndex");
    private final static QName _RemoveTask_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeTask");
    private final static QName _ClearTasks_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "clearTasks");
    private final static QName _FindTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findTaskById");
    private final static QName _FinishTaskByName_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "finishTaskByName");
    private final static QName _UnbindTaskByProject_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "unbindTaskByProject");
    private final static QName _RemoveTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeTaskById");
    private final static QName _FindAllTaskByProjectIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findAllTaskByProjectIdResponse");
    private final static QName _FindOneTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findOneTaskByIndexResponse");
    private final static QName _StartTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "startTaskById");
    private final static QName _FinishTaskByNameResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "finishTaskByNameResponse");
    private final static QName _FindOneTaskByName_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findOneTaskByName");
    private final static QName _RemoveOneTaskByNameResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeOneTaskByNameResponse");
    private final static QName _ChangeTaskStatusByIndexResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "changeTaskStatusByIndexResponse");
    private final static QName _FinishTaskByIndex_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "finishTaskByIndex");
    private final static QName _FinishTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "finishTaskByIdResponse");
    private final static QName _AddTaskByUserId_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "addTaskByUserId");
    private final static QName _StartTaskByNameResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "startTaskByNameResponse");
    private final static QName _RemoveTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeTaskByIdResponse");
    private final static QName _FindAllTasks_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findAllTasks");
    private final static QName _UpdateTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "updateTaskById");
    private final static QName _FindOneTaskByIndex_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findOneTaskByIndex");
    private final static QName _FindOneTaskByNameResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findOneTaskByNameResponse");
    private final static QName _UpdateTaskByIndex_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "updateTaskByIndex");
    private final static QName _FindAllTasksByUserId_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findAllTasksByUserId");
    private final static QName _ChangeTaskStatusByNameResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "changeTaskStatusByNameResponse");
    private final static QName _RemoveOneTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeOneTaskById");
    private final static QName _RemoveOneTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeOneTaskByIndexResponse");
    private final static QName _FindOneTaskById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findOneTaskById");
    private final static QName _AddAllTasksResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "addAllTasksResponse");
    private final static QName _FindAllTasksByUserIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findAllTasksByUserIdResponse");
    private final static QName _UnbindTaskByProjectResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "unbindTaskByProjectResponse");
    private final static QName _ClearTaskByUserIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "clearTaskByUserIdResponse");
    private final static QName _ChangeTaskStatusById_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "changeTaskStatusById");
    private final static QName _RemoveOneTaskByIndex_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeOneTaskByIndex");
    private final static QName _ClearTaskByUserId_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "clearTaskByUserId");
    private final static QName _StartTaskByIndex_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "startTaskByIndex");
    private final static QName _StartTaskByName_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "startTaskByName");
    private final static QName _FindOneTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findOneTaskByIdResponse");
    private final static QName _FindAllTasksResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findAllTasksResponse");
    private final static QName _UpdateTaskByIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "updateTaskByIdResponse");
    private final static QName _UpdateTaskByIndexResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "updateTaskByIndexResponse");
    private final static QName _BindTaskByProjectResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "bindTaskByProjectResponse");
    private final static QName _AddTaskResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "addTaskResponse");
    private final static QName _FindAllTaskByProjectId_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "findAllTaskByProjectId");
    private final static QName _AddTaskByUserIdResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "addTaskByUserIdResponse");
    private final static QName _ClearTasksResponse_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "clearTasksResponse");
    private final static QName _RemoveOneTaskByName_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "removeOneTaskByName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.inshakov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindOneTaskByIndex }
     * 
     */
    public FindOneTaskByIndex createFindOneTaskByIndex() {
        return new FindOneTaskByIndex();
    }

    /**
     * Create an instance of {@link FindOneTaskByNameResponse }
     * 
     */
    public FindOneTaskByNameResponse createFindOneTaskByNameResponse() {
        return new FindOneTaskByNameResponse();
    }

    /**
     * Create an instance of {@link UpdateTaskByIndex }
     * 
     */
    public UpdateTaskByIndex createUpdateTaskByIndex() {
        return new UpdateTaskByIndex();
    }

    /**
     * Create an instance of {@link FindAllTasks }
     * 
     */
    public FindAllTasks createFindAllTasks() {
        return new FindAllTasks();
    }

    /**
     * Create an instance of {@link UpdateTaskById }
     * 
     */
    public UpdateTaskById createUpdateTaskById() {
        return new UpdateTaskById();
    }

    /**
     * Create an instance of {@link RemoveTaskByIdResponse }
     * 
     */
    public RemoveTaskByIdResponse createRemoveTaskByIdResponse() {
        return new RemoveTaskByIdResponse();
    }

    /**
     * Create an instance of {@link StartTaskByNameResponse }
     * 
     */
    public StartTaskByNameResponse createStartTaskByNameResponse() {
        return new StartTaskByNameResponse();
    }

    /**
     * Create an instance of {@link AddTaskByUserId }
     * 
     */
    public AddTaskByUserId createAddTaskByUserId() {
        return new AddTaskByUserId();
    }

    /**
     * Create an instance of {@link FinishTaskByIdResponse }
     * 
     */
    public FinishTaskByIdResponse createFinishTaskByIdResponse() {
        return new FinishTaskByIdResponse();
    }

    /**
     * Create an instance of {@link AddAllTasksResponse }
     * 
     */
    public AddAllTasksResponse createAddAllTasksResponse() {
        return new AddAllTasksResponse();
    }

    /**
     * Create an instance of {@link FindAllTasksByUserIdResponse }
     * 
     */
    public FindAllTasksByUserIdResponse createFindAllTasksByUserIdResponse() {
        return new FindAllTasksByUserIdResponse();
    }

    /**
     * Create an instance of {@link FindOneTaskById }
     * 
     */
    public FindOneTaskById createFindOneTaskById() {
        return new FindOneTaskById();
    }

    /**
     * Create an instance of {@link RemoveOneTaskByIndexResponse }
     * 
     */
    public RemoveOneTaskByIndexResponse createRemoveOneTaskByIndexResponse() {
        return new RemoveOneTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link RemoveOneTaskById }
     * 
     */
    public RemoveOneTaskById createRemoveOneTaskById() {
        return new RemoveOneTaskById();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByNameResponse }
     * 
     */
    public ChangeTaskStatusByNameResponse createChangeTaskStatusByNameResponse() {
        return new ChangeTaskStatusByNameResponse();
    }

    /**
     * Create an instance of {@link FindAllTasksByUserId }
     * 
     */
    public FindAllTasksByUserId createFindAllTasksByUserId() {
        return new FindAllTasksByUserId();
    }

    /**
     * Create an instance of {@link StartTaskByName }
     * 
     */
    public StartTaskByName createStartTaskByName() {
        return new StartTaskByName();
    }

    /**
     * Create an instance of {@link ClearTaskByUserId }
     * 
     */
    public ClearTaskByUserId createClearTaskByUserId() {
        return new ClearTaskByUserId();
    }

    /**
     * Create an instance of {@link StartTaskByIndex }
     * 
     */
    public StartTaskByIndex createStartTaskByIndex() {
        return new StartTaskByIndex();
    }

    /**
     * Create an instance of {@link RemoveOneTaskByIndex }
     * 
     */
    public RemoveOneTaskByIndex createRemoveOneTaskByIndex() {
        return new RemoveOneTaskByIndex();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusById }
     * 
     */
    public ChangeTaskStatusById createChangeTaskStatusById() {
        return new ChangeTaskStatusById();
    }

    /**
     * Create an instance of {@link ClearTaskByUserIdResponse }
     * 
     */
    public ClearTaskByUserIdResponse createClearTaskByUserIdResponse() {
        return new ClearTaskByUserIdResponse();
    }

    /**
     * Create an instance of {@link UnbindTaskByProjectResponse }
     * 
     */
    public UnbindTaskByProjectResponse createUnbindTaskByProjectResponse() {
        return new UnbindTaskByProjectResponse();
    }

    /**
     * Create an instance of {@link RemoveOneTaskByName }
     * 
     */
    public RemoveOneTaskByName createRemoveOneTaskByName() {
        return new RemoveOneTaskByName();
    }

    /**
     * Create an instance of {@link AddTaskByUserIdResponse }
     * 
     */
    public AddTaskByUserIdResponse createAddTaskByUserIdResponse() {
        return new AddTaskByUserIdResponse();
    }

    /**
     * Create an instance of {@link ClearTasksResponse }
     * 
     */
    public ClearTasksResponse createClearTasksResponse() {
        return new ClearTasksResponse();
    }

    /**
     * Create an instance of {@link AddTaskResponse }
     * 
     */
    public AddTaskResponse createAddTaskResponse() {
        return new AddTaskResponse();
    }

    /**
     * Create an instance of {@link FindAllTaskByProjectId }
     * 
     */
    public FindAllTaskByProjectId createFindAllTaskByProjectId() {
        return new FindAllTaskByProjectId();
    }

    /**
     * Create an instance of {@link BindTaskByProjectResponse }
     * 
     */
    public BindTaskByProjectResponse createBindTaskByProjectResponse() {
        return new BindTaskByProjectResponse();
    }

    /**
     * Create an instance of {@link FindAllTasksResponse }
     * 
     */
    public FindAllTasksResponse createFindAllTasksResponse() {
        return new FindAllTasksResponse();
    }

    /**
     * Create an instance of {@link UpdateTaskByIdResponse }
     * 
     */
    public UpdateTaskByIdResponse createUpdateTaskByIdResponse() {
        return new UpdateTaskByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateTaskByIndexResponse }
     * 
     */
    public UpdateTaskByIndexResponse createUpdateTaskByIndexResponse() {
        return new UpdateTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link FindOneTaskByIdResponse }
     * 
     */
    public FindOneTaskByIdResponse createFindOneTaskByIdResponse() {
        return new FindOneTaskByIdResponse();
    }

    /**
     * Create an instance of {@link FinishTaskById }
     * 
     */
    public FinishTaskById createFinishTaskById() {
        return new FinishTaskById();
    }

    /**
     * Create an instance of {@link FinishTaskByIndexResponse }
     * 
     */
    public FinishTaskByIndexResponse createFinishTaskByIndexResponse() {
        return new FinishTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link StartTaskByIdResponse }
     * 
     */
    public StartTaskByIdResponse createStartTaskByIdResponse() {
        return new StartTaskByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskResponse }
     * 
     */
    public RemoveTaskResponse createRemoveTaskResponse() {
        return new RemoveTaskResponse();
    }

    /**
     * Create an instance of {@link AddTask }
     * 
     */
    public AddTask createAddTask() {
        return new AddTask();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByIndex }
     * 
     */
    public ChangeTaskStatusByIndex createChangeTaskStatusByIndex() {
        return new ChangeTaskStatusByIndex();
    }

    /**
     * Create an instance of {@link RemoveTask }
     * 
     */
    public RemoveTask createRemoveTask() {
        return new RemoveTask();
    }

    /**
     * Create an instance of {@link AddAllTasks }
     * 
     */
    public AddAllTasks createAddAllTasks() {
        return new AddAllTasks();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByIdResponse }
     * 
     */
    public ChangeTaskStatusByIdResponse createChangeTaskStatusByIdResponse() {
        return new ChangeTaskStatusByIdResponse();
    }

    /**
     * Create an instance of {@link StartTaskByIndexResponse }
     * 
     */
    public StartTaskByIndexResponse createStartTaskByIndexResponse() {
        return new StartTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByName }
     * 
     */
    public ChangeTaskStatusByName createChangeTaskStatusByName() {
        return new ChangeTaskStatusByName();
    }

    /**
     * Create an instance of {@link BindTaskByProject }
     * 
     */
    public BindTaskByProject createBindTaskByProject() {
        return new BindTaskByProject();
    }

    /**
     * Create an instance of {@link FindTaskByIdResponse }
     * 
     */
    public FindTaskByIdResponse createFindTaskByIdResponse() {
        return new FindTaskByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveOneTaskByIdResponse }
     * 
     */
    public RemoveOneTaskByIdResponse createRemoveOneTaskByIdResponse() {
        return new RemoveOneTaskByIdResponse();
    }

    /**
     * Create an instance of {@link StartTaskById }
     * 
     */
    public StartTaskById createStartTaskById() {
        return new StartTaskById();
    }

    /**
     * Create an instance of {@link FindOneTaskByIndexResponse }
     * 
     */
    public FindOneTaskByIndexResponse createFindOneTaskByIndexResponse() {
        return new FindOneTaskByIndexResponse();
    }

    /**
     * Create an instance of {@link FindAllTaskByProjectIdResponse }
     * 
     */
    public FindAllTaskByProjectIdResponse createFindAllTaskByProjectIdResponse() {
        return new FindAllTaskByProjectIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskById }
     * 
     */
    public RemoveTaskById createRemoveTaskById() {
        return new RemoveTaskById();
    }

    /**
     * Create an instance of {@link UnbindTaskByProject }
     * 
     */
    public UnbindTaskByProject createUnbindTaskByProject() {
        return new UnbindTaskByProject();
    }

    /**
     * Create an instance of {@link FinishTaskByName }
     * 
     */
    public FinishTaskByName createFinishTaskByName() {
        return new FinishTaskByName();
    }

    /**
     * Create an instance of {@link ClearTasks }
     * 
     */
    public ClearTasks createClearTasks() {
        return new ClearTasks();
    }

    /**
     * Create an instance of {@link FindTaskById }
     * 
     */
    public FindTaskById createFindTaskById() {
        return new FindTaskById();
    }

    /**
     * Create an instance of {@link ChangeTaskStatusByIndexResponse }
     * 
     */
    public ChangeTaskStatusByIndexResponse createChangeTaskStatusByIndexResponse() {
        return new ChangeTaskStatusByIndexResponse();
    }

    /**
     * Create an instance of {@link FinishTaskByIndex }
     * 
     */
    public FinishTaskByIndex createFinishTaskByIndex() {
        return new FinishTaskByIndex();
    }

    /**
     * Create an instance of {@link FindOneTaskByName }
     * 
     */
    public FindOneTaskByName createFindOneTaskByName() {
        return new FindOneTaskByName();
    }

    /**
     * Create an instance of {@link RemoveOneTaskByNameResponse }
     * 
     */
    public RemoveOneTaskByNameResponse createRemoveOneTaskByNameResponse() {
        return new RemoveOneTaskByNameResponse();
    }

    /**
     * Create an instance of {@link FinishTaskByNameResponse }
     * 
     */
    public FinishTaskByNameResponse createFinishTaskByNameResponse() {
        return new FinishTaskByNameResponse();
    }

    /**
     * Create an instance of {@link AbstractBusinessEntity }
     * 
     */
    public AbstractBusinessEntity createAbstractBusinessEntity() {
        return new AbstractBusinessEntity();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "addTask")
    public JAXBElement<AddTask> createAddTask(AddTask value) {
        return new JAXBElement<AddTask>(_AddTask_QNAME, AddTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeTaskResponse")
    public JAXBElement<RemoveTaskResponse> createRemoveTaskResponse(RemoveTaskResponse value) {
        return new JAXBElement<RemoveTaskResponse>(_RemoveTaskResponse_QNAME, RemoveTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "startTaskByIdResponse")
    public JAXBElement<StartTaskByIdResponse> createStartTaskByIdResponse(StartTaskByIdResponse value) {
        return new JAXBElement<StartTaskByIdResponse>(_StartTaskByIdResponse_QNAME, StartTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishTaskByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "finishTaskByIndexResponse")
    public JAXBElement<FinishTaskByIndexResponse> createFinishTaskByIndexResponse(FinishTaskByIndexResponse value) {
        return new JAXBElement<FinishTaskByIndexResponse>(_FinishTaskByIndexResponse_QNAME, FinishTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "finishTaskById")
    public JAXBElement<FinishTaskById> createFinishTaskById(FinishTaskById value) {
        return new JAXBElement<FinishTaskById>(_FinishTaskById_QNAME, FinishTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOneTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeOneTaskByIdResponse")
    public JAXBElement<RemoveOneTaskByIdResponse> createRemoveOneTaskByIdResponse(RemoveOneTaskByIdResponse value) {
        return new JAXBElement<RemoveOneTaskByIdResponse>(_RemoveOneTaskByIdResponse_QNAME, RemoveOneTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findTaskByIdResponse")
    public JAXBElement<FindTaskByIdResponse> createFindTaskByIdResponse(FindTaskByIdResponse value) {
        return new JAXBElement<FindTaskByIdResponse>(_FindTaskByIdResponse_QNAME, FindTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindTaskByProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "bindTaskByProject")
    public JAXBElement<BindTaskByProject> createBindTaskByProject(BindTaskByProject value) {
        return new JAXBElement<BindTaskByProject>(_BindTaskByProject_QNAME, BindTaskByProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "changeTaskStatusByName")
    public JAXBElement<ChangeTaskStatusByName> createChangeTaskStatusByName(ChangeTaskStatusByName value) {
        return new JAXBElement<ChangeTaskStatusByName>(_ChangeTaskStatusByName_QNAME, ChangeTaskStatusByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "changeTaskStatusByIdResponse")
    public JAXBElement<ChangeTaskStatusByIdResponse> createChangeTaskStatusByIdResponse(ChangeTaskStatusByIdResponse value) {
        return new JAXBElement<ChangeTaskStatusByIdResponse>(_ChangeTaskStatusByIdResponse_QNAME, ChangeTaskStatusByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartTaskByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "startTaskByIndexResponse")
    public JAXBElement<StartTaskByIndexResponse> createStartTaskByIndexResponse(StartTaskByIndexResponse value) {
        return new JAXBElement<StartTaskByIndexResponse>(_StartTaskByIndexResponse_QNAME, StartTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddAllTasks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "addAllTasks")
    public JAXBElement<AddAllTasks> createAddAllTasks(AddAllTasks value) {
        return new JAXBElement<AddAllTasks>(_AddAllTasks_QNAME, AddAllTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "changeTaskStatusByIndex")
    public JAXBElement<ChangeTaskStatusByIndex> createChangeTaskStatusByIndex(ChangeTaskStatusByIndex value) {
        return new JAXBElement<ChangeTaskStatusByIndex>(_ChangeTaskStatusByIndex_QNAME, ChangeTaskStatusByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeTask")
    public JAXBElement<RemoveTask> createRemoveTask(RemoveTask value) {
        return new JAXBElement<RemoveTask>(_RemoveTask_QNAME, RemoveTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTasks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "clearTasks")
    public JAXBElement<ClearTasks> createClearTasks(ClearTasks value) {
        return new JAXBElement<ClearTasks>(_ClearTasks_QNAME, ClearTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findTaskById")
    public JAXBElement<FindTaskById> createFindTaskById(FindTaskById value) {
        return new JAXBElement<FindTaskById>(_FindTaskById_QNAME, FindTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishTaskByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "finishTaskByName")
    public JAXBElement<FinishTaskByName> createFinishTaskByName(FinishTaskByName value) {
        return new JAXBElement<FinishTaskByName>(_FinishTaskByName_QNAME, FinishTaskByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindTaskByProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "unbindTaskByProject")
    public JAXBElement<UnbindTaskByProject> createUnbindTaskByProject(UnbindTaskByProject value) {
        return new JAXBElement<UnbindTaskByProject>(_UnbindTaskByProject_QNAME, UnbindTaskByProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeTaskById")
    public JAXBElement<RemoveTaskById> createRemoveTaskById(RemoveTaskById value) {
        return new JAXBElement<RemoveTaskById>(_RemoveTaskById_QNAME, RemoveTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTaskByProjectIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findAllTaskByProjectIdResponse")
    public JAXBElement<FindAllTaskByProjectIdResponse> createFindAllTaskByProjectIdResponse(FindAllTaskByProjectIdResponse value) {
        return new JAXBElement<FindAllTaskByProjectIdResponse>(_FindAllTaskByProjectIdResponse_QNAME, FindAllTaskByProjectIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findOneTaskByIndexResponse")
    public JAXBElement<FindOneTaskByIndexResponse> createFindOneTaskByIndexResponse(FindOneTaskByIndexResponse value) {
        return new JAXBElement<FindOneTaskByIndexResponse>(_FindOneTaskByIndexResponse_QNAME, FindOneTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "startTaskById")
    public JAXBElement<StartTaskById> createStartTaskById(StartTaskById value) {
        return new JAXBElement<StartTaskById>(_StartTaskById_QNAME, StartTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishTaskByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "finishTaskByNameResponse")
    public JAXBElement<FinishTaskByNameResponse> createFinishTaskByNameResponse(FinishTaskByNameResponse value) {
        return new JAXBElement<FinishTaskByNameResponse>(_FinishTaskByNameResponse_QNAME, FinishTaskByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findOneTaskByName")
    public JAXBElement<FindOneTaskByName> createFindOneTaskByName(FindOneTaskByName value) {
        return new JAXBElement<FindOneTaskByName>(_FindOneTaskByName_QNAME, FindOneTaskByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOneTaskByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeOneTaskByNameResponse")
    public JAXBElement<RemoveOneTaskByNameResponse> createRemoveOneTaskByNameResponse(RemoveOneTaskByNameResponse value) {
        return new JAXBElement<RemoveOneTaskByNameResponse>(_RemoveOneTaskByNameResponse_QNAME, RemoveOneTaskByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "changeTaskStatusByIndexResponse")
    public JAXBElement<ChangeTaskStatusByIndexResponse> createChangeTaskStatusByIndexResponse(ChangeTaskStatusByIndexResponse value) {
        return new JAXBElement<ChangeTaskStatusByIndexResponse>(_ChangeTaskStatusByIndexResponse_QNAME, ChangeTaskStatusByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishTaskByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "finishTaskByIndex")
    public JAXBElement<FinishTaskByIndex> createFinishTaskByIndex(FinishTaskByIndex value) {
        return new JAXBElement<FinishTaskByIndex>(_FinishTaskByIndex_QNAME, FinishTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "finishTaskByIdResponse")
    public JAXBElement<FinishTaskByIdResponse> createFinishTaskByIdResponse(FinishTaskByIdResponse value) {
        return new JAXBElement<FinishTaskByIdResponse>(_FinishTaskByIdResponse_QNAME, FinishTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "addTaskByUserId")
    public JAXBElement<AddTaskByUserId> createAddTaskByUserId(AddTaskByUserId value) {
        return new JAXBElement<AddTaskByUserId>(_AddTaskByUserId_QNAME, AddTaskByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartTaskByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "startTaskByNameResponse")
    public JAXBElement<StartTaskByNameResponse> createStartTaskByNameResponse(StartTaskByNameResponse value) {
        return new JAXBElement<StartTaskByNameResponse>(_StartTaskByNameResponse_QNAME, StartTaskByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeTaskByIdResponse")
    public JAXBElement<RemoveTaskByIdResponse> createRemoveTaskByIdResponse(RemoveTaskByIdResponse value) {
        return new JAXBElement<RemoveTaskByIdResponse>(_RemoveTaskByIdResponse_QNAME, RemoveTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTasks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findAllTasks")
    public JAXBElement<FindAllTasks> createFindAllTasks(FindAllTasks value) {
        return new JAXBElement<FindAllTasks>(_FindAllTasks_QNAME, FindAllTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "updateTaskById")
    public JAXBElement<UpdateTaskById> createUpdateTaskById(UpdateTaskById value) {
        return new JAXBElement<UpdateTaskById>(_UpdateTaskById_QNAME, UpdateTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findOneTaskByIndex")
    public JAXBElement<FindOneTaskByIndex> createFindOneTaskByIndex(FindOneTaskByIndex value) {
        return new JAXBElement<FindOneTaskByIndex>(_FindOneTaskByIndex_QNAME, FindOneTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findOneTaskByNameResponse")
    public JAXBElement<FindOneTaskByNameResponse> createFindOneTaskByNameResponse(FindOneTaskByNameResponse value) {
        return new JAXBElement<FindOneTaskByNameResponse>(_FindOneTaskByNameResponse_QNAME, FindOneTaskByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "updateTaskByIndex")
    public JAXBElement<UpdateTaskByIndex> createUpdateTaskByIndex(UpdateTaskByIndex value) {
        return new JAXBElement<UpdateTaskByIndex>(_UpdateTaskByIndex_QNAME, UpdateTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTasksByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findAllTasksByUserId")
    public JAXBElement<FindAllTasksByUserId> createFindAllTasksByUserId(FindAllTasksByUserId value) {
        return new JAXBElement<FindAllTasksByUserId>(_FindAllTasksByUserId_QNAME, FindAllTasksByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "changeTaskStatusByNameResponse")
    public JAXBElement<ChangeTaskStatusByNameResponse> createChangeTaskStatusByNameResponse(ChangeTaskStatusByNameResponse value) {
        return new JAXBElement<ChangeTaskStatusByNameResponse>(_ChangeTaskStatusByNameResponse_QNAME, ChangeTaskStatusByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOneTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeOneTaskById")
    public JAXBElement<RemoveOneTaskById> createRemoveOneTaskById(RemoveOneTaskById value) {
        return new JAXBElement<RemoveOneTaskById>(_RemoveOneTaskById_QNAME, RemoveOneTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOneTaskByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeOneTaskByIndexResponse")
    public JAXBElement<RemoveOneTaskByIndexResponse> createRemoveOneTaskByIndexResponse(RemoveOneTaskByIndexResponse value) {
        return new JAXBElement<RemoveOneTaskByIndexResponse>(_RemoveOneTaskByIndexResponse_QNAME, RemoveOneTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findOneTaskById")
    public JAXBElement<FindOneTaskById> createFindOneTaskById(FindOneTaskById value) {
        return new JAXBElement<FindOneTaskById>(_FindOneTaskById_QNAME, FindOneTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddAllTasksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "addAllTasksResponse")
    public JAXBElement<AddAllTasksResponse> createAddAllTasksResponse(AddAllTasksResponse value) {
        return new JAXBElement<AddAllTasksResponse>(_AddAllTasksResponse_QNAME, AddAllTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTasksByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findAllTasksByUserIdResponse")
    public JAXBElement<FindAllTasksByUserIdResponse> createFindAllTasksByUserIdResponse(FindAllTasksByUserIdResponse value) {
        return new JAXBElement<FindAllTasksByUserIdResponse>(_FindAllTasksByUserIdResponse_QNAME, FindAllTasksByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindTaskByProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "unbindTaskByProjectResponse")
    public JAXBElement<UnbindTaskByProjectResponse> createUnbindTaskByProjectResponse(UnbindTaskByProjectResponse value) {
        return new JAXBElement<UnbindTaskByProjectResponse>(_UnbindTaskByProjectResponse_QNAME, UnbindTaskByProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTaskByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "clearTaskByUserIdResponse")
    public JAXBElement<ClearTaskByUserIdResponse> createClearTaskByUserIdResponse(ClearTaskByUserIdResponse value) {
        return new JAXBElement<ClearTaskByUserIdResponse>(_ClearTaskByUserIdResponse_QNAME, ClearTaskByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeTaskStatusById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "changeTaskStatusById")
    public JAXBElement<ChangeTaskStatusById> createChangeTaskStatusById(ChangeTaskStatusById value) {
        return new JAXBElement<ChangeTaskStatusById>(_ChangeTaskStatusById_QNAME, ChangeTaskStatusById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOneTaskByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeOneTaskByIndex")
    public JAXBElement<RemoveOneTaskByIndex> createRemoveOneTaskByIndex(RemoveOneTaskByIndex value) {
        return new JAXBElement<RemoveOneTaskByIndex>(_RemoveOneTaskByIndex_QNAME, RemoveOneTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTaskByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "clearTaskByUserId")
    public JAXBElement<ClearTaskByUserId> createClearTaskByUserId(ClearTaskByUserId value) {
        return new JAXBElement<ClearTaskByUserId>(_ClearTaskByUserId_QNAME, ClearTaskByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartTaskByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "startTaskByIndex")
    public JAXBElement<StartTaskByIndex> createStartTaskByIndex(StartTaskByIndex value) {
        return new JAXBElement<StartTaskByIndex>(_StartTaskByIndex_QNAME, StartTaskByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartTaskByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "startTaskByName")
    public JAXBElement<StartTaskByName> createStartTaskByName(StartTaskByName value) {
        return new JAXBElement<StartTaskByName>(_StartTaskByName_QNAME, StartTaskByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findOneTaskByIdResponse")
    public JAXBElement<FindOneTaskByIdResponse> createFindOneTaskByIdResponse(FindOneTaskByIdResponse value) {
        return new JAXBElement<FindOneTaskByIdResponse>(_FindOneTaskByIdResponse_QNAME, FindOneTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTasksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findAllTasksResponse")
    public JAXBElement<FindAllTasksResponse> createFindAllTasksResponse(FindAllTasksResponse value) {
        return new JAXBElement<FindAllTasksResponse>(_FindAllTasksResponse_QNAME, FindAllTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "updateTaskByIdResponse")
    public JAXBElement<UpdateTaskByIdResponse> createUpdateTaskByIdResponse(UpdateTaskByIdResponse value) {
        return new JAXBElement<UpdateTaskByIdResponse>(_UpdateTaskByIdResponse_QNAME, UpdateTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "updateTaskByIndexResponse")
    public JAXBElement<UpdateTaskByIndexResponse> createUpdateTaskByIndexResponse(UpdateTaskByIndexResponse value) {
        return new JAXBElement<UpdateTaskByIndexResponse>(_UpdateTaskByIndexResponse_QNAME, UpdateTaskByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindTaskByProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "bindTaskByProjectResponse")
    public JAXBElement<BindTaskByProjectResponse> createBindTaskByProjectResponse(BindTaskByProjectResponse value) {
        return new JAXBElement<BindTaskByProjectResponse>(_BindTaskByProjectResponse_QNAME, BindTaskByProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "addTaskResponse")
    public JAXBElement<AddTaskResponse> createAddTaskResponse(AddTaskResponse value) {
        return new JAXBElement<AddTaskResponse>(_AddTaskResponse_QNAME, AddTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTaskByProjectId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "findAllTaskByProjectId")
    public JAXBElement<FindAllTaskByProjectId> createFindAllTaskByProjectId(FindAllTaskByProjectId value) {
        return new JAXBElement<FindAllTaskByProjectId>(_FindAllTaskByProjectId_QNAME, FindAllTaskByProjectId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "addTaskByUserIdResponse")
    public JAXBElement<AddTaskByUserIdResponse> createAddTaskByUserIdResponse(AddTaskByUserIdResponse value) {
        return new JAXBElement<AddTaskByUserIdResponse>(_AddTaskByUserIdResponse_QNAME, AddTaskByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTasksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "clearTasksResponse")
    public JAXBElement<ClearTasksResponse> createClearTasksResponse(ClearTasksResponse value) {
        return new JAXBElement<ClearTasksResponse>(_ClearTasksResponse_QNAME, ClearTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOneTaskByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.inshakov.ru/", name = "removeOneTaskByName")
    public JAXBElement<RemoveOneTaskByName> createRemoveOneTaskByName(RemoveOneTaskByName value) {
        return new JAXBElement<RemoveOneTaskByName>(_RemoveOneTaskByName_QNAME, RemoveOneTaskByName.class, null, value);
    }

}
