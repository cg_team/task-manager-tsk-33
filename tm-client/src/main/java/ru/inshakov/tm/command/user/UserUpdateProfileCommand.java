package ru.inshakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserUpdateProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-update-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "update information about user";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("[UPDATE PROFILE]");
        @Nullable final String userId = session.getUserId();
        System.out.println("ENTER FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().updateUser(userId, firstName, lastName, middleName);
    }

}

