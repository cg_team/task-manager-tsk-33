package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "start project by index";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = serviceLocator.getProjectEndpoint().startProjectByIndex(session, index);
        showProject(project);
    }

}
