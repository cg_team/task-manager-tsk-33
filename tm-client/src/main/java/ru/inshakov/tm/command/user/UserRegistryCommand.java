package ru.inshakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.empty.EmptySessionException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserRegistryCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-registry";
    }

    @NotNull
    @Override
    public String description() {
        return "registry new user in system";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(EmptySessionException::new);
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().createUserWithEmail(login, password, email);
    }

}

