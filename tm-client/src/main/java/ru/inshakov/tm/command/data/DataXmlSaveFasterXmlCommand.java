package ru.inshakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.empty.EmptySessionException;
import ru.inshakov.tm.exception.user.AccessDeniedException;

import java.util.Optional;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "save data from xml file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(EmptySessionException::new);
        serviceLocator.getDataEndpoint().saveDataXmlFasterXmlCommand(session);
    }

}
