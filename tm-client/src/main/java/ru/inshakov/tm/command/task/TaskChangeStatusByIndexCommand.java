package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.endpoint.Status;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status by index";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID");
        @NotNull final Integer index = TerminalUtil.nextNumber()-1;
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusId);
        @Nullable final Task task = serviceLocator.getTaskEndpoint().changeTaskStatusByIndex(session, index, status);
    }

}
