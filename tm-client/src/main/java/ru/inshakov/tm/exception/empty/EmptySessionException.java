package ru.inshakov.tm.exception.empty;

public final class EmptySessionException extends RuntimeException {

    public EmptySessionException() {
        super("Error! You must be logged in...");
    }

}
